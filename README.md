# README #

This README would normally document whatever steps are necessary to get your application up and running.

## DMVNO WIKI ##
http://35.200.79.61:8080/DigitalMVNO

### server info ###
* 기본적인 service 내부 DNS : http://{App명}-svc.default.svc.cluster.local:{port} -- app명의 '-'나 '_'는 제외

* config server : http://configserver-svc.default.svc.cluster.local:8888  -- 내부접속
* ui server : http://a5d5613a28e6011e9bc2e0a3f756ff94-608613856.ap-northeast-2.elb.amazonaws.com  -- 외부접속              
* gateway : http://a819dbefea6d111e9a5260241e1ca044-2045704601.ap-northeast-2.elb.amazonaws.com:8000 -- 외부접속
            http://gateway-svc.default.svc.cluster.local:8000 -- 내부접속
* kibana : http://a4e1a4351924e11e9a5260241e1ca044-1128898448.ap-northeast-2.elb.amazonaws.com:5601 -- 외부접속
* zipkin : http://13.125.233.137:9411

### port info ###
* customer 8080
* balance 8081
* core 8083
* interface_receive 8084
* interface_send 8085
* means_real 8087
* means_vrbank 8088
* means_card 8089

* gateway 8000
* config-server 8888
* service-discovery 8761
* webserver 9090

* legacy user 8078
* legacy means 8079

### What is this repository for? ###

DMVNO config-server config information

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
